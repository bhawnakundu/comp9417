import pandas as pd
import re
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from math import sqrt
import chart_studio.plotly as py
import plotly.offline as pyoff
import plotly.graph_objs as go
import keras
from keras.layers import Dense
from keras.models import Sequential
from keras.optimizers import Adam 
from keras.callbacks import EarlyStopping
from keras.utils import np_utils
from keras.layers import LSTM
from pmdarima import auto_arima
import warnings

warnings.filterwarnings("ignore")
#conda install -c saravji pmdarima  
#conda install -c conda-forge keras
#conda install -c plotly plotly 
#conda install -c plotly chart-studio

plt.style.use("ggplot")

"""Loading the CSV files"""
def load_data(fileName):
	
	df = pd.read_csv("dataset/"+fileName)
	
	return df


"""Total Unit Sales on Weekdays for last 5.4 Years"""
def unitSalesTotal(sales_df):
	
	salesWeedaysTotal = sales_df[['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']].sum(axis=0)

	new_df = salesWeedaysTotal.to_frame()

	som = new_df.plot(kind='barh', figsize=(10,10), legend=None)

	for i, label in enumerate(list(new_df.index)):
	    som.annotate(str(new_df.loc[label][0]), (i, new_df.loc[label][0]), ha='center', va='bottom')

	for i in som.patches:
		som.text(i.get_height()-3, i.get_y(), \
	            str(round((i.get_width()), 2)), color='yellow')

	plt.ylabel("Days")
	plt.xlabel("Total Sales for last 5.4 years(in Millions)")
	plt.savefig("ComparisionOfSalesPerWeekday.png")


""" Total units sold per state for last 5.4 years"""
def totalUnitsSoldPerState(sales_df):

	new_df = sales_df.groupby(['state_id'])["totalUnitsSold"].sum(axis=0)

	new_df = new_df.to_frame()
	new_df=new_df.reset_index()

	unival = new_df["totalUnitsSold"]

	colors = ["#E13F29", "#D69A80", "#D63B59"]

	unival.plot.pie(subplots=True, autopct='%1.1f%%', labels=new_df['state_id'], colors=colors, startangle=90, radius=0.6, figsize=(8, 8), title = "Sales By State", shadow = False, explode=(0.09, 0, 0))
	plt.savefig("totalSalesPerState.png")


""" Total units sold per store for last 5.4 years"""
def totalUnitsSoldPerStore(sales_df):

	new_df = sales_df.groupby(['store_id'])["totalUnitsSold"].sum(axis=0)

	new_df = new_df.to_frame()
	som = new_df.plot(kind='bar', figsize=(8,8), legend=None, color=[plt.cm.Paired(np.arange(len(new_df)))])

	for i, label in enumerate(list(new_df.index)):
	    som.annotate(str(new_df.loc[label][0]), (i, new_df.loc[label][0]), ha='center', va='bottom')

	for i in som.patches:
		som.text(i.get_height()-3, i.get_y(), \
	            str(round((i.get_width()), 2)), color='black')

	plt.ylabel("Sales(per million units)")
	plt.xlabel("Stores")
	plt.savefig("totalUnitsSoldPerStore.png")


""" Total units sold per category for last 5.4 years"""
def totalUnitsSoldPerCategory(sales_df):

	new_df = sales_df.groupby(['cat_id'])["totalUnitsSold"].sum(axis=0)

	new_df = new_df.to_frame()
	new_df=new_df.reset_index()

	unival = new_df["totalUnitsSold"]

	colors = ["#CB3119", "#D04A80", "#D64B59"]

	unival.plot.pie(subplots=True, autopct='%1.1f%%', labels=new_df['cat_id'], colors=colors, startangle=90, radius=0.6, figsize=(8, 8), title = "Units Sold Per Category", shadow = False, explode=(0.09, 0, 0))
	plt.savefig("totalSalesPerState.png")


""" Total units sold per year for last 5.4 years"""
def totalUnitsSoldPerYear(sales_df):

	salesYearTotal = sales_df[['totalUnitsSoldInYear2011', 'totalUnitsSoldInYear2012', 'totalUnitsSoldInYear2013', 'totalUnitsSoldInYear2014', 'totalUnitsSoldInYear2015', 'totalUnitsSoldInYear2016']].sum(axis=0)

	new_df = salesYearTotal.to_frame()

	new_df.plot(figsize=(8, 8), linewidth=2.5, color='maroon', legend=None)
	plt.xlabel("Year", labelpad=10)
	plt.ylabel("Units sold (in millions)", labelpad=10)
	plt.xticks(rotation=25)
	plt.title("Total Unit Sales Per year", y=1.0, fontsize=16);
	plt.savefig("ComparisionOfUnitsSoldPerYear.png")


""" Total sales per year for last 5.4 years"""
def totalSalesPerYearPerStore(sales_df):

	salesYearTotal11 = sales_df.groupby(['store_id'])['salesYear2011'].sum(axis=0).to_frame().rename(columns={'salesYear2011':'2011'})
	salesYearTotal12 = sales_df.groupby(['store_id'])['salesYear2012'].sum(axis=0).to_frame().rename(columns={'salesYear2012':'2012'})
	salesYearTotal13 = sales_df.groupby(['store_id'])['salesYear2013'].sum(axis=0).to_frame().rename(columns={'salesYear2013':'2013'})
	salesYearTotal14 = sales_df.groupby(['store_id'])['salesYear2014'].sum(axis=0).to_frame().rename(columns={'salesYear2014':'2014'})
	salesYearTotal15 = sales_df.groupby(['store_id'])['salesYear2015'].sum(axis=0).to_frame().rename(columns={'salesYear2015':'2015'})
	salesYearTotal16 = sales_df.groupby(['store_id'])['salesYear2016'].sum(axis=0).to_frame().rename(columns={'salesYear2016':'2016'})

	data_frames = [salesYearTotal11, salesYearTotal12, salesYearTotal13,salesYearTotal14,salesYearTotal15, salesYearTotal16]
	df_merged = reduce(lambda  left,right: pd.merge(left,right,on=['store_id'],
                                            how='outer'), data_frames)

	df_merged.T.plot(figsize=(8, 8))
	plt.xlabel("year", labelpad=10)
	plt.ylabel("total sales per year (in US$)", labelpad=9)
	plt.legend(loc="upper right", title="Store id")
	plt.savefig("totalSalesPerStore.png")


# Add new columns of weekdays with units sold
def addWeekDaysToDF(calendar_df, sales_df):

	sales_df = sales_df.assign(**{'sunday': 0, 'monday': 0, 'tuesday': 0, 'wednesday': 0, 'thursday': 0, 'friday': 0, 'saturday': 0})

	sunday_array, monday_array, tuesday_array, wednesday_array, thursday_array, friday_array, saturday_array = [],[],[],[],[],[],[]

	for col in sales_df.columns:
		if (re.match("d_.*", col)):
			print(col)
			for index, cal_rows in calendar_df.iterrows():
				if cal_rows['d'] == col:
					day = cal_rows['weekday']
			
					if day == "Sunday": 
						sunday_array.append(col)
					elif day == "Monday":
						monday_array.append(col)
					elif day == "Tuesday":
						tuesday_array.append(col)
					elif day == "Wednesday":
						wednesday_array.append(col)
					elif day == "Thursday":
						thursday_array.append(col)
					elif day == "Friday":
						friday_array.append(col)
					elif day == "Saturday":
						saturday_array.append(col)
					else:
						print("ERROR wrong day name")
					
					break


	col_list= list(sunday_array)
	sales_df['sunday'] = sales_df[col_list].sum(axis=1)
	col_list= list(monday_array)
	sales_df['monday'] = sales_df[col_list].sum(axis=1)
	col_list= list(tuesday_array)
	sales_df['tuesday'] = sales_df[col_list].sum(axis=1)
	col_list= list(wednesday_array)
	sales_df['wednesday'] = sales_df[col_list].sum(axis=1)
	col_list= list(thursday_array)
	sales_df['thursday'] = sales_df[col_list].sum(axis=1)
	col_list= list(friday_array)
	sales_df['friday'] = sales_df[col_list].sum(axis=1)
	col_list= list(saturday_array)
	sales_df['saturday'] = sales_df[col_list].sum(axis=1)

	return sales_df


# Add new columns with prices of items
def addPricesForUnits(sell_price_df, calendar_df, sales_df):

	flag = 0
	daysPerYear = []
	currYear = prevYear = 0
	for i, rows in calendar_df.iterrows():

		if (calendar_df.index[-1] == i):
			newcol = "totalUnitsSoldInYear"+str(prevYear)
			sales_df[newcol]= sales_df[daysPerYear].sum(axis=1)
			break

		if str(rows['d']) in sales_df.columns:
			if (flag==0):
				currYear = rows['year']
				prevYear = rows['year']
				daysPerYear.append(rows['d'])
				flag=1
			else:
				currYear = rows['year']

				if currYear != prevYear:
					newcol = "totalUnitsSoldInYear"+str(prevYear)
					sales_df[newcol] = sales_df[daysPerYear].sum(axis=1)

					daysPerYear = []
					prevYear = currYear
					daysPerYear.append(rows['d'])
				else:

					daysPerYear.append(rows['d'])

	# find average for subset of years
	calendar_new_df = calendar_df[["wm_yr_wk","year", "d"]]
	sell_price_df = pd.merge(sell_price_df, calendar_new_df, how='left', on=['wm_yr_wk'])

	# average sell price each year each item each store
	temp = sell_price_df.groupby(['item_id','year', 'store_id'])['sell_price'].mean().round(2)
	temp = temp.to_frame()

	# merge sales and sell price df(temp) with average units sold and average sell price each year respectively
	temp = temp.pivot_table('sell_price', ['item_id', 'store_id'], 'year')
	temp = temp.rename(columns = {2011:'sellPrice2011', 2012:'sellPrice2012', 2013:'sellPrice2013', 2014:'sellPrice2014', 2015:'sellPrice2015', 2016:'sellPrice2016' })

	sales_df = pd.merge(sales_df, temp, on = ["item_id", "store_id"], how = "right").fillna(0)

	# mean all the sell prices for all the years
	new_df = sell_price_df.groupby(['store_id', 'item_id'])['sell_price'].mean().round(2)

	# merge sales and sell to get new sales with prices added to df
	sales_df = pd.merge(sales_df,new_df,  how = 'left', on = ['store_id','item_id'])

	sales_df["salesYear2011"] = sales_df["totalUnitsSoldInYear2011"] * sales_df["sellPrice2011"]
	sales_df["salesYear2012"] = sales_df["totalUnitsSoldInYear2012"] * sales_df["sellPrice2012"]
	sales_df["salesYear2013"] = sales_df["totalUnitsSoldInYear2013"] * sales_df["sellPrice2013"]
	sales_df["salesYear2014"] = sales_df["totalUnitsSoldInYear2014"] * sales_df["sellPrice2014"]
	sales_df["salesYear2015"] = sales_df["totalUnitsSoldInYear2015"] * sales_df["sellPrice2015"]
	sales_df["salesYear2016"] = sales_df["totalUnitsSoldInYear2016"] * sales_df["sellPrice2016"]
	sales_df["salesTotal"] = sales_df["totalUnitsSold"]*sales_df["sell_price"]


	########### Uncomment these functions to see the plots############
	# totalUnitsSoldPerState(sales_df)
	# totalUnitsSoldPerYear(sales_df)
	# totalUnitsSoldPerStore(sales_df)
	# totalUnitsSoldPerCategory(sales_df)
	# totalSalesPerYearPerStore(sales_df)
	return sales_df

# convert full integer to Million
def million(x, pos):
	return '%1.1fM' % (x * 1e-7)

# Split the 'd_*' days id
def split_it(day):
    return int(re.findall('(\d.*)', day)[0])


# New columns per week sales(unit * price)/pre-process and manipulation
# Merges all the csv to make one df.
def processdata(sell_price_df, calendar_df, sales_df):

	flag = 0
	daysPerYear = []
	currYear = prevYear = 0

	temp_sales_df = sales_df

	for i, rows in calendar_df.iterrows():
		print(rows['d']+" out of d_1969")
		if (calendar_df.index[-1] == i):
			newcol = str(prevYear)
			temp_sales_df[newcol]= temp_sales_df[daysPerYear].sum(axis=1)
			break

		if str(rows['d']) in temp_sales_df.columns:
			if (flag == 0):
				currYear = rows['wm_yr_wk']
				prevYear = rows['wm_yr_wk']
				daysPerYear.append(rows['d'])
				flag = 1
			else:
				currYear = rows['wm_yr_wk']

				if currYear != prevYear:
					newcol = str(prevYear)
					temp_sales_df[newcol] = temp_sales_df[daysPerYear].sum(axis=1)

					daysPerYear = []
					prevYear = currYear
					daysPerYear.append(rows['d'])
				else:

					daysPerYear.append(rows['d'])	

	calendar_new_df = calendar_df[["wm_yr_wk","year", "d"]]
	sell_price_df = pd.merge(sell_price_df, calendar_new_df, how = 'left', on = ['wm_yr_wk'])

	# average sell price each year each item each week all store
	temp = sell_price_df.groupby(['item_id','wm_yr_wk'])['sell_price'].mean().round(2)
	temp = temp.to_frame()

	# merge sales and sell price df(temp) with average units sold and average sell price each year respectively
	temp =temp.pivot_table('sell_price', ['item_id'], 'wm_yr_wk').fillna(0).reset_index()

	# change name to string which were int after pivot
	temp.columns = temp.columns.map(str)

	cols_to_use = temp_sales_df.columns.intersection(temp.columns)

	temp_sales_df = pd.merge(temp_sales_df, temp[cols_to_use], on = ["item_id"], how="inner").fillna(0)

	temp_sales_df = temp_sales_df.rename(columns=lambda x: re.sub('_[xy]','',x))

	lol = temp_sales_df.groupby(temp_sales_df.columns, axis = 1).prod().reset_index()
	
	cols = [c for c in lol.columns if c.isdigit()]
	lol = lol[cols]
	lol = lol.transpose()
	lol = lol.sum(axis=1).reset_index(name ='totalAmount')

	return lol



#Plot a line graph for data before finding the difference in the weekly sales(non-stationary)
def plotBeforeDifference(sales_diff):
	
	x = sales_diff['index']
	y = sales_diff['totalAmount']
	plot_data = [
	    go.Scatter(
	        x = x,
	        y = y,
	    )
	]
	fig = go.Figure(data = plot_data)



#Plot a line graph for data after finding the difference in the weekly sales(stationary check)
def plotAfterDifference(sales_diff):
	
	x = sales_diff['index']
	y = sales_diff['difference']
	plot_data = [
	    go.Scatter(
	        x = x,
	        y = y,
	    )
	]
	fig = go.Figure(data = plot_data)



#Plot a line graph(2 lines) for predicted and actual weekly sales
def plotActualAndPredictedValues(df_sales_pred_plot_against, df_sales_pred_actual, df_sales_pred_predicted):
	
	plot_data = [
	    go.Scatter(
	        x = df_sales_pred_plot_against,
	        y = df_sales_pred_actual,
	        name ='actual'
	    ),
        go.Scatter(
	        x = df_sales_pred_plot_against,
	        y = df_sales_pred_predicted,
	        name='predicted'
	    )    
	]
	plot_layout = go.Layout(
	        title = 'Sales Prediction'
	    )
	fig = go.Figure(data = plot_data, layout = plot_layout)
	fig.show()



#Predits the weekly sales for the next 4 weeks using Linear regression model 
def linearReg(lol, calendar_df):
	
	sales_diff = lol.copy()
	sales_diff = sales_diff.rename(columns={'index': 'wm_yr_wk'})
	calendar_df = calendar_df.groupby(['wm_yr_wk'])['event_name_1'].count().reset_index()
	
	calendar_df['wm_yr_wk'] = calendar_df['wm_yr_wk'].apply(str)
	sales_diff['wm_yr_wk'] = sales_diff['wm_yr_wk'].apply(str)
	sales_diff = pd.merge(sales_diff, calendar_df, on='wm_yr_wk', how='left')
	
	# Applying the model
	model = LinearRegression()
	train_set, test_set = sales_diff[0:-4], sales_diff.tail(4)
	
	# Splitting the data into test and train
	X_train = train_set.drop('totalAmount', axis = 1)
	y_train = train_set['totalAmount']
	X_valid = test_set.drop('totalAmount', axis = 1)
	y_valid = test_set['totalAmount']
	
	model.fit(X_train,y_train)
	
	preds = model.predict(X_valid)
	forecast = pd.DataFrame(preds,index = test_set.index, columns = ['LinearRegPrediction'])
	forecast = pd.merge(forecast, test_set, left_index = True, right_index = True, how = "left")
	
	plotActualAndPredictedValues(forecast['wm_yr_wk'], forecast['totalAmount'], forecast['LinearRegPrediction'])

	# Calculating RMSE
	RMSE = np.sqrt(np.mean(np.power((np.array(y_valid)-np.array(preds)),2)))
	print("RMSE(LinearRegression): "+str(RMSE))
	
	return forecast



# Predits the weekly sales for the next 4 weeks using Auto ARIMA model
def autoArima(lol, calendar_df):
	
	sales_diff = lol.copy()

	sales_diff = sales_diff.rename(columns={'index': 'wm_yr_wk'})
	calendar_df = calendar_df.groupby(['wm_yr_wk'])['event_name_1'].count().reset_index()
	calendar_df['wm_yr_wk'] = calendar_df['wm_yr_wk'].apply(str)
	sales_diff['wm_yr_wk'] = sales_diff['wm_yr_wk'].apply(str)
	sales_diff = pd.merge(sales_diff, calendar_df, on = 'wm_yr_wk', how = 'left')
	
	train_set, test_set = sales_diff[0:-4], sales_diff.tail(4)
	
	training = train_set['totalAmount']
	validation = test_set['totalAmount']

	model = auto_arima(training, trace = True, error_action = 'ignore', suppress_warnings = True)
	model.fit(training)

	forecast = model.predict(n_periods = 4)
	forecast = pd.DataFrame(forecast, index = test_set.index, columns = ['AutoARIMAPrediction'])
	forecast = pd.merge(forecast, test_set, left_index = True, right_index = True, how = "left")

	plotActualAndPredictedValues(forecast['wm_yr_wk'], forecast['totalAmount'], forecast['AutoARIMAPrediction'])

	# Calculating RMSE
	RMSE = np.sqrt(np.mean(np.power((np.array(test_set['totalAmount']) - np.array(forecast['AutoARIMAPrediction'])),2)))
	print("RMSE (Auto ARIMA): "+str(RMSE))

	return forecast



# Predits the weekly sales for the next 4 weeks using LSTM model
def predict_sales_LSTM(lol):

	sales_df = sales_diff = lol.copy()
	
	plotBeforeDifference(sales_diff)
	
	sales_diff['previousSales'] = sales_diff['totalAmount'].shift(1)
	sales_diff = sales_diff.dropna()
	sales_diff['difference'] = (sales_diff['totalAmount'] - sales_diff['previousSales'])

	plotAfterDifference(sales_diff)

	sales_supervised = sales_diff.drop(['previousSales'],axis=1)

	# looking at data for the last 52 weeks
	makestringofWeeks = ""
	
	for i in range(1,53):
	    field_name = 'week_' + str(i)
	    if makestringofWeeks == "":
	    	makestringofWeeks = "difference ~ "+field_name+""
	    else:
	    	makestringofWeeks = makestringofWeeks+" + "+field_name
	    
	    sales_supervised[field_name] = sales_supervised['difference'].shift(i)

	sales_supervised = sales_supervised.dropna().reset_index(drop=True)

	temp = sales_supervised.drop(['totalAmount','index'],axis=1)
	
	# split sales_diff to train and test set
	sales_train_set, sales_test_set = temp[0:-4].values, temp.tail(4).values

	scalerData = MinMaxScaler(feature_range=(-1, 1))
	scalerData = scalerData.fit(sales_train_set)
	
	sales_train_set = sales_train_set.reshape(sales_train_set.shape[0], sales_train_set.shape[1])
	train_set_scaled = scalerData.transform(sales_train_set)
	
	sales_test_set = sales_test_set.reshape(sales_test_set.shape[0], sales_test_set.shape[1])
	test_set_scaled = scalerData.transform(sales_test_set)

	X_train, y_train = train_set_scaled[:, 1:], train_set_scaled[:, 0:1]
	X_train = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])
	X_test, y_test = test_set_scaled[:, 1:], test_set_scaled[:, 0:1]
	X_test = X_test.reshape(X_test.shape[0], 1, X_test.shape[1])

	model = Sequential()
	model.add(LSTM(units = 4, batch_input_shape = (1, X_train.shape[1], X_train.shape[2]), stateful = True))
	model.add(Dense(1))
	model.compile(loss='mean_squared_error', optimizer='adam')
	model.fit(X_train, y_train, nb_epoch = 100, batch_size = 1, verbose = 1, shuffle = False)
	y_pred = model.predict(X_test, batch_size = 1)

	#reshape y_pred
	y_pred = y_pred.reshape(y_pred.shape[0], 1, y_pred.shape[1])
	
	#rebuild test set for inverse transform
	pred_test_set = []
	
	for index in range(0,len(y_pred)):
	    pred_test_set.append(np.concatenate([y_pred[index],X_test[index]],axis=1))
	
	#reshape pred_test_set
	pred_test_set = np.array(pred_test_set)
	pred_test_set = pred_test_set.reshape(pred_test_set.shape[0], pred_test_set.shape[2])
	
	#inverse transform
	pred_test_set_inverted = scalerData.inverse_transform(pred_test_set)

	#create dataframe that shows the predicted sales
	pred_list = []
	sales_dates = list(sales_df[-5:]["index"])
	
	act_sales = list(sales_df[-5:].totalAmount)
	
	for index in range(0,len(pred_test_set_inverted)):
	    
	    dict_pred = {}
	    dict_pred['pred_value'] = int(pred_test_set_inverted[index][0] + act_sales[index])
	    dict_pred['index'] = sales_dates[index + 1]
	    pred_list.append(dict_pred)
	
	result = pd.DataFrame(pred_list)

	df_sales_pred = pd.merge(sales_df, result, on = 'index', how = 'left')
	
	#plot actual and predicted values
	plotActualAndPredictedValues(df_sales_pred['index'], df_sales_pred['totalAmount'], df_sales_pred['pred_value'])

	df_sales_pred = df_sales_pred.dropna()

	RMSE = sqrt(mean_squared_error(df_sales_pred.totalAmount, df_sales_pred.pred_value))

	print("RMSE (LSTM): " + str(RMSE))
	
	return df_sales_pred


# running all the models
def predictions():

	sales_df = load_data("sales_train_evaluation.csv")
	sales_df["totalUnitsSold"] = sales_df.sum(axis=1)
	
	calendar_df = load_data("calendar.csv")
	sell_price_df = load_data("sell_prices.csv")
	
	processedData = processdata(sell_price_df, calendar_df, sales_df)

	predictionsLSTM = predict_sales_LSTM(processedData)
	predictionsLinearReg = linearReg(processedData, calendar_df)
	predictionsArima = autoArima(processedData, calendar_df)

	print(predictionsLSTM)
	print(predictionsLinearReg)
	print(predictionsArima)

if __name__ == "__main__":
   predictions()



