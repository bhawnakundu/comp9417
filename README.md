# README #

### Set up and Installation (Libraries required) ###

* pip3 install pandas
* pip3 install numpy

* Download Anaconda for your OS or use pip3 install to install libraries below.

* $conda install -c anaconda scikit-learn
* $conda install -c saravji pmdarima  
* $conda install -c conda-forge keras
* $conda install -c plotly plotly 
* $conda install -c plotly chart-studio

### How to run ###

* python3 forecast.py

### Files description ###

* Code for ML 	: forecast.py
* dataset folder	: datasets for ML

* Plots			: Sample plot images(.png)
* ExtraDatasets	: Used for personal purposes(reduces time - to run the whole thing)

### Authors ###

* Bhawna Kundu z5130378
* Sunreet Singh z5130780